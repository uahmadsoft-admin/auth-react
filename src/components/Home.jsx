import React, { useEffect, useState } from 'react'
import axios from 'axios'
const Home = () => {

  let token = localStorage.getItem('token');
  const [data,setData]= useState([]);
  useEffect( ()=>{
    axios.get('http://localhost:8080/api/hello',
      {
      headers: { Authorization: `Bearer ${token}`}
      }
    )
    .then( res => setData(res.data));
  },[token])
  return (
    <div>
      {data.map( website =>(
        <div key={website._id}>
          <p>{website.url} --- <span>{website.website}</span></p>
        </div>
      ))}
    </div>
  )
}

export default Home;