import React from 'react'
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { logout } from '../redux/loginReducer';
const NavBar = () => {
    const user = useSelector(state=>state.user);
    const dispatch = useDispatch();
 return (
    <div>
        <NavLink to="/home"> Home</NavLink>
        <NavLink to="/websites"> Websites</NavLink>

        {user.user.user === 'admin' && (<NavLink to="/user"> User</NavLink>)}
        <NavLink to="/" onClick={()=> dispatch(logout()) }>Logout</NavLink>
    </div>
  )
}

export default NavBar