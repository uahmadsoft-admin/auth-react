import { configureStore } from '@reduxjs/toolkit'
import verifyUser from './loginReducer'; 
export default configureStore({
    reducer: {
        user: verifyUser
    },
})

